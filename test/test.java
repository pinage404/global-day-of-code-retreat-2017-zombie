import chai from 'chai'
chai.should()

public class Live {
	public Live() {
		this.grid = new Array()
	}

	public void add(cell) {
		this.grid.push(cell)
	}


	public Live live() {
		return [
			new Cell(0, 0, true, 0)
		]
	}
}


class Cell {
	constructor(x, y, already_dead, alive) {
		this.x = x
		this.y = y
		this.already_dead = already_dead
		this.alive = alive
	}
}

describe('', () => {
	it('should add a cell', () => {
		const cell = new Cell(0, 0, false, 1)
		const live = new Live()

		live.add(cell)

		live.grid.should.be.deep.equal([
			cell,
		])
	})

	it('should add 2 cell', () => {
		const cell = new Cell(0, 0, false, 1)
		const cell2 = new Cell(0,1,false , 1)
		const live = new Live()

		live.add(cell2)
		live.add(cell)

		live.grid.should.be.deep.equal([
			cell2,
			cell,
		])
	})

	
	it('should dead if there are no neighbour', () => {
		const cell = new Cell(0, 0, false, 1)
		const live = new Live()
		live.add(cell)

		const nextGrid = live.live()

		nextGrid.should.be.deep.equal([
			new Cell(0, 0, true, 0 )
		])
	})

	/*
	it('should be alive if there is 3 living', () => {
		const live = new Live()
		live.add(new Cell(0, 0, false, 1))
		live.add(new Cell(1, 0, false, 1))
		live.add(new Cell(0, 1, false, 1))
		live.add(new Cell(1, 1, false, 0))
		

		const nextGrid = live.live()

		nextGrid.should.be.deep.equal([
			new Cell(1, 1, false, 1),
		])
	})
**/
});
